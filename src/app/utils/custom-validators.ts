import {FormControl, ValidatorFn} from '@angular/forms';

export class CustomValidators {
  static readonly NAME_1CHR_VALIDATOR_PATTERN = '^[A-Za-z]+$';
  static readonly NAME_VALIDATOR_PATTERN = '^[a-zA-Z\\s]+[a-zA-Z\\s\'\\-]*[a-zA-Z\\s]+$';

  static checkNameCharacters: ValidatorFn = (control: FormControl) => {
    const name: string = control.value;
    if (name && name !== '') {
      if (name.length === 1) {
        return name.match(CustomValidators.NAME_1CHR_VALIDATOR_PATTERN) == null ? {nameInvalid: true} : null;
      } else {
        return name.match(CustomValidators.NAME_VALIDATOR_PATTERN) == null ? {nameInvalid: true} : null;
      }
    }
    return null;
  }
}
