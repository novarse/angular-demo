import {Injectable} from '@angular/core';
import {Observable, of} from 'rxjs';
import {MatSnackBar} from '@angular/material/snack-bar';
import {HttpErrorResponse} from '@angular/common/http';

@Injectable({
  providedIn: 'root'
})
export class UtilsService {

  constructor(private snackBar: MatSnackBar) {
  }

  public handleError<T>(operation = 'operation', result?: T) {
    return (error: HttpErrorResponse): Observable<T> => {
      let msg = error.error.error;
      if (msg) {
      } else {
        msg = error.error.message;
      }
      if (msg) {
      } else {
        msg = error.statusText;
      }

      console.error(msg); // log to console instead
      this.snackBar.open(msg, '', {duration: 5000, panelClass: ['red-snackbar']});

      return of(result as T);
    };
  }

}
