import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';

import { AppRoutingModule } from './app-routing.module';
import { AppComponent } from './app.component';
import { HomeComponent } from './components/home/home.component';
import { InputFormComponent } from './components/input-form/input-form.component';
import { NewsComponent } from 'src/app/components/news/news.component';
import { MainLayoutComponent } from './components/main-layout/main-layout.component';
import { NavigationComponent } from './components/main-layout/navigation/navigation.component';
import { FooterComponent } from './components/main-layout/footer/footer.component';
import { HeaderComponent } from './components/main-layout/header/header.component';
import {ReactiveFormsModule} from '@angular/forms';
import { BrowserAnimationsModule } from '@angular/platform-browser/animations';
import {MatFormFieldModule} from '@angular/material/form-field';
import {MatInputModule} from '@angular/material/input';
import {MatButtonModule} from '@angular/material/button';
import {FlexModule} from '@angular/flex-layout';
import {HTTP_INTERCEPTORS, HttpClientModule} from '@angular/common/http';
import {HeaderInterceptor} from 'src/app/interceptors/header-interceptor';
import {HttpService} from 'src/app/services/http.service';
import {MatSnackBarModule} from '@angular/material/snack-bar';
import {MatDatepickerModule} from '@angular/material/datepicker';
import {MAT_MOMENT_DATE_ADAPTER_OPTIONS, MatMomentDateModule} from '@angular/material-moment-adapter';
import {MAT_DATE_LOCALE} from '@angular/material/core';
import {MatCardModule} from '@angular/material/card';
import {MatTooltipModule} from '@angular/material/tooltip';
import {NewsLineComponent} from 'src/app/components/news/news-line/news-line.component';
import {MatIconModule} from '@angular/material/icon';

@NgModule({
  declarations: [
    AppComponent,
    HomeComponent,
    InputFormComponent,
    NewsComponent,
    MainLayoutComponent,
    NavigationComponent,
    FooterComponent,
    HeaderComponent,
    NewsLineComponent
  ],
  imports: [
    HttpClientModule,
    BrowserModule,
    AppRoutingModule,
    ReactiveFormsModule,
    BrowserAnimationsModule,
    MatFormFieldModule,
    MatInputModule,
    MatButtonModule,
    MatSnackBarModule,
    FlexModule,
    MatDatepickerModule,
    MatMomentDateModule,
    MatCardModule,
    MatTooltipModule,
    MatIconModule
  ],
  providers: [
    HttpService,
    {provide: HTTP_INTERCEPTORS, useClass: HeaderInterceptor, multi: true},
    { provide: MAT_DATE_LOCALE, useValue: 'en-AU' },
    { provide: MAT_MOMENT_DATE_ADAPTER_OPTIONS, useValue: { useUtc: true } }
  ],
  bootstrap: [AppComponent]
})
export class AppModule { }
