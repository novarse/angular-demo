export class DetailDto {
  givenName: string;
  surname: string;
  email: string;
  birthdate: string;

  constructor(givenName?: string,
              surname?: string,
              email?: string,
              birthdate?: string) {
    this.givenName = givenName;
    this.surname = surname;
    this.email = email;
    this.birthdate = birthdate;
  }
}
