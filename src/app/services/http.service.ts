import { Injectable } from '@angular/core';
import {HttpClient} from '@angular/common/http';
import {Observable} from 'rxjs';
import {BaseResponse} from 'src/app/dto/base-response';
import {NewsDto} from 'src/app/dto/news-dto';

@Injectable({
  providedIn: 'root'
})
export class HttpService {

  constructor(private http: HttpClient) { }

  postDetails(body: string): Observable<any> {
    return this.http.post<BaseResponse>('/public/demo/details/save', body);
  }

  getNews(): Observable<NewsDto[]> {
    return this.http.get<NewsDto[]>('/public/home/news');
  }
}
