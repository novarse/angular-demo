import { Component, OnInit } from '@angular/core';
import {FormBuilder, FormGroup, Validators} from '@angular/forms';
import {HttpService} from 'src/app/services/http.service';
import {catchError, tap} from 'rxjs/operators';
import {MatSnackBar} from '@angular/material/snack-bar';
import {DetailDto} from 'src/app/dto/detail-dto';
import {CustomValidators} from 'src/app/utils/custom-validators';
import {UtilsService} from 'src/app/utils/utils.service';

@Component({
  selector: 'app-input-form',
  templateUrl: './input-form.component.html',
  styleUrls: ['./input-form.component.css']
})
export class InputFormComponent implements OnInit {
  readonly MIN_BIRTH_DATE = new Date(1950, 0, 2);
  readonly END_DATE = new Date(2006, 11, 32);
  readonly EMAIL_PATTERN = '^[_A-Za-z0-9-\\+]+(\\.[_A-Za-z0-9-]+)*@'
    + '[A-Za-z0-9-]+(\\.[A-Za-z0-9]+)*(\\.[A-Za-z]{2,})$';

  form: FormGroup;
  submitted = false;

  constructor(private fb: FormBuilder,
              private snackBar: MatSnackBar,
              private utilsService: UtilsService,
              private http: HttpService) { }

  ngOnInit(): void {
    this.form = this.fb.group({
      givenName: [null, [Validators.required, CustomValidators.checkNameCharacters]],
      surname: [null, [Validators.required, CustomValidators.checkNameCharacters]],
      email: [null, [Validators.required, Validators.pattern(this.EMAIL_PATTERN)]],
      birthdate: [null, Validators.required]
    });
  }

  onSubmit() {
    this.form.markAllAsTouched();
    if (this.form.valid) {
      this.http.postDetails(this.buildBody())
        .pipe(
          tap(() => {
            this.snackBar.open('Submitted OK', '', {duration: 4000, panelClass: 'green-snackbar'});
            this.submitted = true;
          }),
          catchError(this.utilsService.handleError())
        )
        .subscribe();
    }
  }

  onReset() {
    this.form.reset();
    this.submitted = false;
  }

  private buildBody() {
    return JSON.stringify(new DetailDto(
      this.form.get('givenName').value,
      this.form.get('surname').value,
      this.form.get('email').value,
      (new Date(this.form.get('givenName').value)).toJSON()
    ));
  }

}
