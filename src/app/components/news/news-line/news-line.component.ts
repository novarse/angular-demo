import {Component, EventEmitter, Input, OnInit, Output} from '@angular/core';
import {NewsDto} from 'src/app/dto/news-dto';

@Component({
  selector: '[app-news-line]',
  templateUrl: './news-line.component.html',
  styleUrls: ['./news-line.component.css']
})
export class NewsLineComponent implements OnInit {

  @Input()
  news: NewsDto;

  @Output()
  msgFeedBack = new EventEmitter<string>();

  constructor() { }

  ngOnInit(): void {
  }

  onFeedMsgBack(messageShort: string) {
    this.msgFeedBack.emit(messageShort);
  }
}
