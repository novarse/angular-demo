import {Component, OnInit} from '@angular/core';
import {FormBuilder, FormGroup} from '@angular/forms';
import {MatSnackBar} from '@angular/material/snack-bar';
import {HttpService} from 'src/app/services/http.service';
import {tap} from 'rxjs/operators';
import {NewsDto} from 'src/app/dto/news-dto';
import {BehaviorSubject} from 'rxjs';

@Component({
  selector: 'app-news',
  templateUrl: './news.component.html',
  styleUrls: ['./news.component.css']
})
export class NewsComponent implements OnInit {

  form: FormGroup;

  news: NewsDto[];
  loaded = new BehaviorSubject<boolean>(false);

  constructor(private fb: FormBuilder,
              private snackBar: MatSnackBar,
              private http: HttpService) {
  }

  ngOnInit(): void {
    this.form = this.fb.group({});

    this.initNews();
  }


  private initNews() {
    this.http.getNews()
      .pipe(
        tap(data => {
          this.news = data;
          this.loaded.next(true);
        })
      )
      .subscribe();
  }

  onMsgFeedBack($event: string) {
    this.snackBar.open($event,  '', {duration: 4000, panelClass: 'green-snackbar'});
  }
}
