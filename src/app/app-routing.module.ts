import {InputFormComponent} from 'src/app/components/input-form/input-form.component';

import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import {HomeComponent} from 'src/app/components/home/home.component';
import {NewsComponent} from 'src/app/components/news/news.component';
import {MainLayoutComponent} from 'src/app/components/main-layout/main-layout.component';


const routes: Routes = [
  {
    path: '',
    component: MainLayoutComponent,
    children: [
      {
        path: '',
        redirectTo: 'home',
        pathMatch: 'full'

      },
      {
        path: 'home',
        component: HomeComponent
      },
      {
        path: 'input-form',
        component: InputFormComponent
      },
      {
        path: 'news',
        component: NewsComponent
      }
    ]
  }
];


@NgModule({
  imports: [RouterModule.forRoot(routes, { useHash: true})],
  exports: [RouterModule]
})
export class AppRoutingModule { }
